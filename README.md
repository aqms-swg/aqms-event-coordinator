:no_entry_sign: **CAUTION, WORK IN PROGRESS** :no_entry_sign:
# Event Coordinator  

Event coordinator (ec) parses hypoinverse messages from hyps2ps (and potentially other sources such as trig2db); populates the database parametric information tables with this event information; and publishes CMS to notify subscribing modules about the new or updated event. ec tries to determine if the inputs from multiple sources are describing the same seismic event; and if so, to combine those inputs into a single database event and set of output signals.

ec can be compiled to parse the duration magnitude information from hypoinverse messages and to populate that information into the parametric information tables. This option would be appropriate if your Earthworm system is configured to provide duration magnitudes along with hypocenter information in the hypoinverse message.

## How ec works  


Event Coordinator collects event information from multiple sources, associates these into a single event, writes the event information to the database, and notifies downstream modules of the event. The event sources can include hypocenter information from binder_ew/hypoinverse, and triggers from evtdetect? (large amplitude detector) and ​carlsubtrig (coincidence subnet triggers) [NOT USED]. Event cancellation messages can also be processed.

Event Coordinator processes input whenever it receives it from CMS connections. It also processes known events on a time schedule. To facilitate the processing schedule, events are kept in an EventList in order of their next-due processing time, soonest due event at top of the list.

Hypocenter input (from hyps2ps) includes most of the origin and arrival information (and optionally coda duration and Md values) from the hypoinverse archive message. This information is loaded into a Solution structure, leaving the TriggerList empty. The locevid is taken from the binder ID number. The Solution is passed to DoEvent() with the solType set to Hyp.

Trigger information from evtdetect? (via evt2ps?) includes a trigger ID, the trigger time, and the name of the first station to trigger. This station location is placed in the Solution Origin structure with quality set to 0.25. The single station is put in the TriggerList. Also, the station trigger time is placed in the Origin arrival list as a P phase with quality 0.5, unknown first motion. The trigger ID is assigned to the Origin locevid. The Solution is passed to DoEvent() with the solType set to Evtrig.

**Not Currently Used:** Trigger information from carlsubtrig (via trig2ps) includes a trigger ID, the trigger time, and the list of all the triggering stations. The location of the first triggering station is placed in the Solution Origin structure with quality set to 0.25. All the triggering stations are put in the TriggerList. All the triggers are also put in the Origin arrival list as P phases with quality 0.5 and unknown first motion. The trigger ID is assigned to the Origin locevid. The Solution is passed to DoEvent() with the solType set to Subtrig.

Event Coordinator executes its Run procedure whenever the wakeup time is reached. The wakeup time is set in the Transition procedure, depending on the new state of the event.

**DoEvent procedure:**

All solTypes are treated the same; the solType argument is used only to set the order of Solutions in the event SolutionList.

DoEvent does the following steps: 

```
    Is the new solution an update to an existing solution of the same
    solType, locevid and source?
    If yes, and the current state of that event is Prelim {
        Replace existing solution in event SolutionList with new solution.
        Remove event from EventList
        If this event is due for update {
            Transition (see below) event as Timeout.
        }
        else {
            reinsert in EventList
            Send PrelimUpdate message (event location with locFinal flag FALSE)
        }
    }
    else if Yes and current event state is not Prelim: ignore event message
    else {
        Can this solution be associated with another current Prelim event?
        For this test, an event must have:
            A valid solution (solution not cancelled)
            Candidate event preferred solution (first in solution list) and
            new solution time windows must overlap:
                window start times are solution origin times
                window end times are start times plus FinalEventDelay (90 sec)
            New solution must be different solType than the preferred event
            solution.
        If new solution can be associated {
            Remove event from EventList
            Insert new solution in event SolutionList in solType order.
            Is event due for update?
            If yes {
                Transition (see below) event as Timeout.
            }
            else {
                Reinsert event in EventList
                Send PrelimUpdate message (event location with locFinal flag FALSE)
            }
        }
        If new solution cannot be associated {
            Create new Event structure
            Get new evid from database sequence
            Transition (see below) event as New.
        }
    }
    Set wakeup time from event at top of EventList.
```

On solution Cancel message arrival:  

```
    Parse Cancel message for locevid, auth, subsource, program.
    Search solutions of events in EventList for match of locevid, auth,
    subsource and program.
    If event is found {
        Remove event from EventList
        If event state was Prelim and its preferred solution is being cancelled
        {
            If event has another valid solution {
                Reinsert event in EventList.
                Send PrelimUpdate message (event location with locFinal flag FALSE)
            }
            else {
                Transition (see below) event as Cancelled.
            }
        }
    }
    Set wakeup time from event at top of EventList.
```  

Transition procedure; given Event and transition type  

```
    Get next state from Event Transition Table (below)
    switch (nextState) {   no "drop-through" from one case to next.
    Prelim:
        Set event state to Prelim
        Send PrelimUpdate message (event location with locFinal flag FALSE)
        Next wakeup time for this event:
            origin time + FinalEventDelay (90 sec)
        Reinsert event in EventList
    Timeout:
        Set event state to Final
        Write event to database.
            Tables written are Event, Origin, Arrival, AssocArO
            Only the first origin in SolutionList is written
            This origin becomes the preferred origin for this event.
            Note that this is the ONLY time event is written to database in
            Event Coordinator.
        If database write is successful, send Event Signal (evid only)
        Send FinalUpdate message (event location with locFinal flag TRUE)
        Next wakeup time for this event:
            origin time + FinalDelay (90 sec) + PurgeEventDelay (5 min)
        Reinsert event in EventList
    Cancelled:
        Set event state to Cancelled
        Send EventCancelled signal (evid, auth, subsource)
        Next wakeup time for this event:
            origin time + FinalEventDelay (90 sec) + PurgeEventDelay (5 min)
        Reinsert event in EventList
    Purged:
        Set event state to Purged
        Do not insert event in Eventlist; this is how the event gets dropped
    Error or New:
        Set event state to Error
        Do not insert event in Eventlist; this is how the event gets dropped
    }
```  

**Run procedure :**   

```
    For each event whose wakeup time has occurred:
        Transition event as Timeout
```

## How hyps2ps works  

hyps2ps takes hypoinverse archive messages from an Earthworm ring and feeds the messages to CMS subscribers.

Due to limitations in the CMS system, hyps2ps does not actually put the hypoinverse message into CMS. Instead, hyps2ps writes the message to a file in a configured directory. Then hyps2ps publishes a CMS message that contains the path to that file.

ec is the only program that may subscribe to the CMS messages from hyps2ps.
