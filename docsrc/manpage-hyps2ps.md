# hyps2ps {#man-hyps2ps}

hyps2ps takes hypoinverse archive messages from an Earthworm ring and feeds the messages to CMS subscribers.  


## PAGE LAST UPDATED ON

2021-01-27  

## NAME

hyps2ps  

## VERSION and STATUS

v0.3.4 2013-12-19 - Md Coda's processing enabled  
status : ACTIVE

## PURPOSE

hyps2ps takes hypoinverse archive messages from an Earthworm ring and feeds the messages to CMS subscribers.

Due to limitations in the CMS system, hyps2ps does not actually put the hypoinverse message into CMS. Instead, hyps2ps writes the message to a file in a configured directory. Then hyps2ps publishes a CMS message that contains the path to that file.

*ec* is the only program that may subscribe to the CMS messages from hyps2ps.  


## HOW TO RUN

Since hyps2ps is an earthworm module, it can be run under startstop using the following command:   

```
hyps2ps hyps2ps.d
```

## CONFIGURATION FILE   

hyps2ps.d is described below.  

Format of the config file is : 

<parameter name> <data type> <default value>
<description>

**MyModuleId** *string*  
Specify the hyps2ps module name from earthworm.d. REQUIRED

**RingName** *string*  
Specify the Earthworm transport ring name from earthworm.d. REQUIRED

**LogFile** *integer*  
Specify the Earthworm logging level. REQUIRED

* 0 turns off logging to file; logging to STDERR is on.
* 1 turns on logging to file and to STDERR.
* 2 turns off logging to STDERR; logging to file is on.  

**HeartBeatInterval** *integer*  
Specify the interval, in seconds, between heartbeat messages. REQUIRED

**NetworkID** *string*  
Specify the Network ID for hyps2ps to pass to ec, where it will be inserted into the database Auth fields of the various parametric tables. REQUIRED  

**SourceID** *string*  
Specify the Source ID for hyps2ps to pass to ec, where it will be inserted into the database Subsource fields of the various parametric tables. REQUIRED  

**LocAlgo** *string*  
Specify the location algorithm for hyps2ps to pass to ec, where it will be inserted into the database algorithm field of the Origin table. REQUIRED

**DurMagAlgo** *string*  
Specify the duration magnitude algorithm for hyps2ps to pass to ec, where it will be inserted into the database algorithm field of the Netmag table. OPTIONAL

**GetHypsFrom** *string* *string*  
Specify the Earthworm Installation ID(s) and Module ID(s) of messages that hyps2ps is to read from the transport ring. (The Message ID is hard-coded to TYPE_HYP2000ARC.) You can use up to five GetHypsFrom commands; at least one is required.

**MsgConfig** *string*  
Specify the CMS config file to be used by hyps2ps. REQUIRED

**MsgSubject** *string*  
Specify the subject of the CMS message that is published by hyps2ps. Match this subject with the one to which ec subscribes. REQUIRED

**MsgDirectory** *string*  
Specify the directory into which hyps2ps will place the hypoinverse archive files to be read by ec.


A sample hyps2ps.d is shown below :  
```
#
# This is the hyps2ps parameter file
#

#  Basic Earthworm setup:
#
MyModuleId         MOD_HYP_PUB # module id for this instance of template 
RingName           HYPO_RING    # shared memory ring for input/output
LogFile            1            # 0 to completely turn off disk log file
HeartBeatInterval  30           # seconds between heartbeats
NetworkID	   NC
SourceID 	   RT1
LocAlgo		   BINDER
DurMagAlgo	   Pick_ew/Hypoinverse
#
#
# List the message logos to grab from transport ring
#              Installation       Module          Message Types
GetHypsFrom   INST_UCB        MOD_WILDCARD    # archive message
#
# Message System configuration file
MsgConfig	/home/ncss/run/params/cms.cfg
MsgSubject	/parameters/hypocenters/binder/archive
MsgDirectory	/home/ncss/run/hyp_msgs
```


## DEPENDENCIES

hyps2ps is an Earthworm module and thus must have Earthworm transport rings configured.

hyps2ps reads hypoinverse archive messages (Earthworm message TYPE_HYP2000ARC), so the Earthworm module ​hyp2000mgr and its supporting programs must be writing these messages into the transport ring from which hyps2ps reads.

hyps2ps publishes to CMS.

hyps2ps writes files to a directory named by the MsgDirectory configuration file command, and sends the full path name of those files to ec. ec must be able to access those files through that exact pathname. That means that either hyps2ps and ec must run on the same computer, or the filesystem containing that directory must be shared in such a way that both programs can access the directory with the same path.


## MAINTENANCE

No specific advice other than to check the log file.  


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-event-coordinator/-/issues  

## MORE INFORMATION

Authors : Patrick Small, Kalpesh Solanki, Paul Friberg, Peter Lombard  

[Earthworm module documentation](​http://www.earthwormcentral.org/documentation4/modules.html)

