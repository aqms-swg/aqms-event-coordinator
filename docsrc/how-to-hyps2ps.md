How to compile and install hyps2ps {#how-to-compile-install-hyps2ps}
====================================

# Pre-requisites

hyps2ps depends on aqms-libs, so ensure that they are compiled and available before compiling it.  It also depends on Earthworm libraries.

# Compile

```
make -f Makefile
```
This will create a binary named *hyps2ps* and *hypsub*.

# Install

Since hyps2ps is an earthworm module, it can be run under *startstop*. Earthworm documentation can be found at http://www.earthwormcentral.org/documentation4/index.html

hyps2ps must have Earthworm transport rings configured.

hyps2ps writes files to a directory named by the MsgDirectory configuration file command, and sends the full path name of those files to ec (event coordinator). ec must be able to access those files through that exact pathname. That means that either hyps2ps and ec must run on the same computer, or the filesystem containing that directory must be shared in such a way that both programs can access the directory with the same path.


