How to compile and install ec {#how-to-compile-install-ec}
====================================

# Pre-requisites

ec depends on aqms-libs, so ensure that they are compiled and available before compiling it.  It also depends on Earthworm libraries.

# Compile

```
make -f Makefile
```
This will create a binary named *ec*.

# Install

At your preferred location, create a folder named *ec*. Copy binary *ec* under this.  
Under folder *ec* create a folder named *configs*. Copy the ec config files here.



# Configure

Modify the template *ec.cfg* as per your equirements.

Ensure that *Logfile* is available and writable to by *ec*.

Ensure that the database is running and reachable by *ec*.