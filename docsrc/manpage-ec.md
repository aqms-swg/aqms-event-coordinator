# ec {#man-ec}



## PAGE LAST UPDATED ON

2021-01-27

## NAME

ec

## VERSION and STATUS

v2.4.4 2018-08-16 Md Coda's processing enabled  
status : ACTIVE
           

## PURPOSE

ec parses hypoinverse messages from hyps2ps (and potentially other sources such trig2db); populates the database parametric information tables with this event information; and publishes CMS to notify subscribing modules about the new or updated event. ec tries to determine if the inputs from multiple sources are describing the same seismic event; and if so, to combine those inputs into a single database event and set of output signals.

ec can be compiled to parse the duration magnitude information from hypoinverse messages and to populate that information into the parametric information tables. This option would be appropriate if your Earthworm system is configured to provide duration magnitudes along with hypocenter information in the hypoinverse message.


## HOW TO RUN

```
ec  ec.cfg
```  

where the *ec.cfg* is described below. Note that ec also writes to STDOUT and STDERR, so it may be desirable to capture that output with the conlog program.     

## CONFIGURATION FILE

Format of configuration files is :  

<parameter name> <data type> <default value>
<description>

*ec.cfg* has the following arguments :  

**General configuration parameters**  

**CMSConfig** *string* cms.cfg  
Points to the cms configuration file which holds all CMS details. In addition, the common CMS signals should be in the config file.  

**SystemSubject** *string*  

**StatusSubject** *string**  

**DebugSubject** *string**  

**InfoSubject** *string*  

**ErrorSubject** *string*  

**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored.  

**LoggingLevel** *integer*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  

**ProgramName** *string* EVENT_COORD  
The unique name to allow this module to connect to CMS and be identified.   

**HSInterval** *integer*  
How often in seconds to send a heartbeat signal to CMS.  
  

**Application configuration parameters**  
  

**Auth** *string*  
The 2 character code of the authority running the ec module (e.g., CI, NC, HI).  

**Subsource** *string*  
Specify the Source ID for ec to insert into the database Subsource field of the Event table (e.g. RT1) 

** SourceCodetoSubSource **
An alternative to just a straight SubSource assignment. This uses SourceCode char from hypoinverse and is case sensitive. The idea here is to allow different earthworm pipelines to have different subsource values (i.e. to differentiate Machine Learning from say regular Earthworm derived solutions).
Examples are: 
SourceCodeToSubSource  M=RT1-ML  
SourceCodeToSubSource  W=RT1


**HypocenterSubject** *string*  
Specify the subject of the hypocenter CMS message to which ec will subscribe. This is the subject of messages published by hyps2ps.  

**CancelSubject** *string*  
Specify the subject of hypocenter cancellation message to which ec will subscribe. This is the subject of messages published by hyps2ps. ec can differentiate between hypocenter and cancellation messages of the same subject since they are different message types.

**EventTriggerSubject** *string*  
Specify the subject of the event trigger CMS message to which ec will subscribe. This is the subject of messages published by evt2ps???. If you don't use evtdetect??? or evt2ps???, you could set this subject to none.

**SubnetTriggerSubject** *string*   
Specify the subject of the subnet trigger CMS message to which ec will subscribe. This is the subject of messages published by trig2db. It is highly recommended that ec not subscribe to messages from trig2db. Therefore you should set SubnetTriggerSubject to none.

**HypQualityFile** *string*  
Specify the hypocenter quality file pathname that ec will read. This command is required, and the file must exist and be accessible by the given pathname from the directory in which ec will run. ec does not use this file for any purpose; this is what is known as vapor-ware.

**EvTrigQualityFile** *string*  
Specify the event trigger quality file pathname that ec will read. This command is required, and the file must exist and be accessible by the given pathname from the directory in which ec will run. ec does not use this file for any purpose.  

**SubTrigQualityFile** *string*  
Specify the subnet trigger quality file pathname that ec will read. This command is required, and the file must exist and be accessible by the given pathname from the directory in which ec will run. ec does not use this file for any purpose.

**FinalEventDelay** *integer*  
Specify final delay time in seconds.  

**PurgeEventDelay** *integer*  
Specify purge delay time in seconds.  

**PrelimEventSubject** *string*  
Specify the subject of the preliminary CMS message that ec will publish. This message contains hypocenter and phase information.  

**FinalEventSubject** *string*  
Specify the subject of the final CMS message that ec will publish. This message contains hypocenter and phase information.

**EventSignalSubject** *string*  
Specify the subject of the event signal message that ec will publish. This message consists of the event ID only.

**EventCancelSubject** *string*  
Specify the subject of the event cancel signal message that ec will publish. This message consists of the event ID only.

**PrefMagChar** *string*  
Allows the user to OPTIONALLY specify the preferred magnitude character. This defaults to D if not specified.  

**DBService** *string*  
Specfy the name of the database being used.

**DBUser** *string*  
Specify the name of the database user account.

**DBPasswd** *string*  
Specify the password for the database user account specified by DBUser.

**DBConnectionRetryInterval** *integer*  
Specify the interval (in arbitrary units) for attempts to reconnect to the datease. While this command is required, it has no effect on the behavior of ec; more vapor-ware.

**DBMaxConnectionRetries** *integer*  
Specify the number of times to attempt to reconnect to the database. While this command is required, it has no effect on the behavior of ec.


**Optional parameters for Quicklook processing**   


**UseOriginVersion** *integer*  
Optional command to use the origin version number to make decisions about "quicklook" processing. Set n to 1 to turn on this option; set n to 0 or remove this command to disable quicklook processing. Quicklook processing means that an origin with version number 0 will be immediately inserted into the database, and a message with QuicklookSignalSubject will be published with the event ID. The version 0 origin message will typically be produced by eqassemble??? with its PrelimRule??? activated. If a subsequent origin message with the same local event ID from the same subsource is received, it will be processed according to the standard event coordinator rules. If no subsequent origin message with the same local event ID from the same subsource is received, or if an event cancellation message for that event ID is received, then event coordinator will publish the event ID under subject CancelSignalSubject, and the event.selectflag will be set to 0 in the database.

**QuicklookDelay** *integer*  
Optional command (required if UseOriginVersion is set to 1). Specifies the delay in seconds to wait after the version 0 origin message before a subsequent origin or cancellation with the same local event ID from the same subsurce. If no subsequent message is received by delay_seconds after the Origin time, then event coordinator will cancel the event.

**QuicklookSignalSubject** *string*  
Optional command (required if UseOriginVersion is set to 1). Specifies the subject under which the quicklook message is published.

**CancelSignalSubject** *string*  
Optional command (required if UseOriginVersion is set to 1). Specifies the subject under which the quicklook cancellation messages is published.  

**LocalRegion** *string* <region_name>  
Optional parameter for determining "local" vs "regional" event locations, where <region-name> is the name of a region in the Gazetteer_Region database table. You can use as many LocalRegion commands as you want.  With no LocalRegion's specified, all earthquakes will be "local".  

A sample *ec.cfg* is shown below.  
```
#
# General configuration parameters
#
CMSConfig       ./cms.cfg
SystemSubject   /system/control
StatusSubject   /process/reports/status
DebugSubject    /process/reports/debug
InfoSubject     /process/reports/info
ErrorSubject    /process/reports/error
Logfile         ./ec
LoggingLevel    2
ProgramName     EVENTCOORD
HSInterval      30


#
# Application configuration parameters
#
Auth				CI
Subsource			RT3
FinalEventDelay			90
# PurgeEventDelay is in MINUTES; other delays are in seconds
PurgeEventDelay			5
HypQualityFile			./ec_hypquality.cfg
EvTrigQualityFile		./ec_evtrigquality.cfg
SubTrigQualityFile		./ec_subtrigquality.cfg
HypocenterSubject               /parameters/hypocenters/binder_ew/archive
CancelSubject                   /parameters/hypocenters/binder_ew/cancel
EventTriggerSubject             /parameters/detections/evtdetect
SubnetTriggerSubject            /parameters/detections/carlsubtrig
PrelimEventSubject              /parameters/events/eventcoord
FinalEventSubject               /parameters/events/eventcoord
EventSignalSubject              /signals/eventcoord/event
EventCancelSubject              /parameters/events/eventcoord/cancel
DBService                       some_dbname
DBUser                          some_user
DBPasswd                        some_password
DBConnectionRetryInterval       5
DBMaxConnectionRetries          5

#
# Optional parameters for Quicklook processing
# If UseOriginVersion is 1, then the remaining 
# parameters are required.
UseOriginVersion		1
QuicklookDelay			300
QuicklookSignalSubject		/signals/eventcoord/quicklook
CancelSignalSubject		/signals/eventcoord/cancel


# optional prefmag in hypoinverse selection
# use the prefmag from hypoinverse if chars match one of the following:
# comma separated list (no spaces) of allowed pref mag chars.
#
#PrefMagChar D,Y

#
# Optional parameter for determining "local" vs "regional" event locations.
# LocalRegion    <region-name> 
#  where <region-name> is the name of a region in the Gazetteer_Region
#  database table. You can use as many LocalRegion commands as you want.
#  With no LocalRegion's specified, all earthquakes will be "local".
LocalRegion   NC
```
  
## DEPENDENCIES

ec uses the CMS system.

ec reads files using the pathname published by hyps2ps. ec must be able to access those files through that published pathname. That means that either hyps2ps and ec must run on the same computer, or the filesystem containing that directory must be shared in such a way that both programs can access the directory with the same path.


## MAINTENANCE

No specific advice other than to check the log file. 

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-event-coordinator/-/issues

## MORE INFORMATION

Authors : Patrick Small, Kalpesh Solanki, Paul Friberg, Peter Lombard 
