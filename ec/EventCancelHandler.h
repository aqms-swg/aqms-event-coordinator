/**
* @file
* @ingroup group_event-coordinator_ec
* @brief Header for EventCancelHandler.C
*/
/***********************************************************

File Name :
        EventCancelHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_EVENT_CANCEL messages.


Creation Date:
        02 December 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef event_cancel_handler_H
#define event_cancel_handler_H


// Various include files
#include "Connection.h"


// Function prototype for Event Location message handler
void handleEventCancelMsg(Message &m, void *arg);


#endif
