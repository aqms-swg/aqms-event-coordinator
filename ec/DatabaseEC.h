/**
* @file
* @ingroup group_event-coordinator_ec
* @brief Header for DatabaseEC.C
*/
/***********************************************************

File Name :
        DatabaseEC.h

Original Author:
        Patrick Small

Description:


Creation Date:
        31 August 1999

Modification History:
	06 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef database_ec_H
#define database_ec_H

// forward declaration
class EventCoord;

// Various include files
#include <vector>
#include <stdint.h>
#include "EventCoord.h"
#include "Database.h"
#include "Event.h"
#include "Channel.h"

using namespace std;

class DatabaseEC : public Database
{
 private:

    int _IsPrimary();
    int _WriteArrivals(Origin &ori, vector<int> &arrids, int totarr);
    int _WriteOrigin(Event &e, Origin &ori);
    int _WriteAssocARO(Origin &ori, vector<int> &arrids, ArrivalList &al);

#ifdef CODAS_FROM_PICKEW
    int _WriteMagnitude(uint32_t evid, Origin &org, Magnitude &mag, 
			int use_magpref_stored_procs);
    int _WriteCodas(Origin &org, ChanCodaList &cl, vector<int> &coids, 
		    int totcoda);
    int _WriteAssocCoM(Origin &org, uint32_t &prefmag,
		       vector<int> &coids, CodaList &cl);
    int _WriteAssocCoO(Origin &org, vector<int> &coids, CodaList &cl);
#endif    

 protected:

 public:
    DatabaseEC();
    DatabaseEC(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseEC();

    int GetNextEventID(uint32_t &evid);
    int GetLocation(Channel &chan);
    int WriteEvent(Event &e, int use_magpref_stored_procs,
		   int is_update = 0);
    int DeleteEvent(uint32_t &evid);
    int SetGtype(Origin &org, EventCoord *ec);
};

#endif
