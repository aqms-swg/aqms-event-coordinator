/**
* @file
* @ingroup group_event-coordinator_ec
*/
/***********************************************************

File Name :
        DeclaredEvent.h

Original Author:
        Patrick Small

Description:


Creation Date:
        23 November 1999

Modification History:
	07 September 2009 Pete Lombard Added Quicklook and New_Quicklook states
	06 Dec 2016	- ADG: 64-bit system compliance updates performed

Usage Notes:

**********************************************************/

#ifndef declared_event_H
#define declared_event_H

// Various include files
#include <map>
#include <stdint.h>
#include "Solution.h"

// Definition of the maximum number of event states
const int MAX_EVENT_STATES = 8;

// Definition of the possible event states
//typedef enum {EVENT_STATE_NONE, EVENT_STATE_PRELIM, EVENT_STATE_FINAL,
//	      EVENT_STATE_CANCELLED, EVENT_STATE_PURGED} eventState;
// Note that EVENT_STATE_PURGED must always be the LAST value in this enumeration.
typedef enum {EVENT_STATE_ERROR, EVENT_STATE_NEW, EVENT_STATE_PRELIM, 
	      EVENT_STATE_FINAL, EVENT_STATE_CANCELLED, 
	      EVENT_STATE_NEW_QUICKLOOK, EVENT_STATE_QUICKLOOK,
	      EVENT_STATE_PURGED} eventState;

// String labels for each event state
const char eventStateStrings[MAX_EVENT_STATES][16] = {"Error", "New", 
                                                      "Preliminary",
						      "Final", "Cancelled",
						      "New_Quicklook",
						      "Quicklook",
						      "Purged"};

// Definition of the possible solution types, in order of preference
typedef enum {SOL_TYPE_HYP, SOL_TYPE_EVTRIG, SOL_TYPE_SUBTRIG} solType;

// Maximum number of event types
const int SOL_MAX_TYPE = 3;

// String descriptions of each solution type
const char solTypeStrings[SOL_MAX_TYPE][32] = {"Hypocenter", 
					       "Event Trigger", 
					       "Subnet Trigger"};


// Definition of a solution list. The list is sorted in order of
// preferred solution type.
typedef std::multimap<int, Solution, std::less<int> > SolutionList;


class DeclaredEvent
{
 private:

 protected:

 public:
    uint32_t evid;
    eventState curstate;
    SolutionList sols;
    int modcount;
    int eventInDatabase;

    DeclaredEvent();
    DeclaredEvent(const DeclaredEvent &e);
    ~DeclaredEvent();

    DeclaredEvent& operator=(const DeclaredEvent &e);
};

#endif
