/**
* @file
* @ingroup group_event-coordinator_ec
* @brief Header for SubnetTrigHandler.C
*/
/***********************************************************

File Name :
        SubnetTrigHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_SUBNET_TRIG messages.


Creation Date:
        15 November 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef subnet_trig_handler_H
#define subnet_trig_handler_H


// Various include files
#include "Connection.h"


// Function prototype for Subnet Trigger message handler
void handleSubnetTrigMsg(Message &m, void *arg);


#endif
