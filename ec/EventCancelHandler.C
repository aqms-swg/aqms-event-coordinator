/**
* @file
* @ingroup group_event-coordinator_ec
* @brief Defines the routines needed to process incoming TN_TMT_EVENT_CANCEL messages
*/
/***********************************************************

File Name :
        EventCancelHandler.C

Original Author:
        Patrick Small

Description:

        This source file defines the routines needed to process
incoming TN_TMT_EVENT_CANCEL messages.


Creation Date:
        02 December 1999

Modification History:
	06 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

// Various include files
#include <iostream>
#include "RetCodes.h"
#include "PackageAssocCancel.h"
#include "EventCancelHandler.h"
#include "EventCoord.h"


// Callback for Event Cancel messages
void handleEventCancelMsg(Message &m, void *arg)
{
  EventCoord *ec;
  RTStatusManager sm;
  struct assocCancel cancel;
  char locevid[MAXSTR];

  ec = (EventCoord *)arg;
  if (ec->GetStatusManager(sm) != TN_SUCCESS) {
    std::cout << "Error (handleEventCancelMsg): Unable to retrieve status manager" 
	 << std::endl;
    return;
  }

  sm.InfoMessage("Received cancel message...");

  // Unpack the message containing the hypocenter
  if (unpackAssocCancel(cancel, m) != TN_SUCCESS) {
    sm.ErrorMessage("(handleEventCancelMsg): Unable to retrieve cancellation");
    return;
  }

  sprintf(locevid, "%d", cancel.locid);

  // Invoke the Event Coordinator to process the cancellation message
  ec->DoCancel(locevid, cancel.source, cancel.subsource, cancel.loc_algo);
  return;
}
