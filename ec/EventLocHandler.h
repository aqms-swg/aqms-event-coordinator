/**
* @file
* @ingroup group_event-coordinator_ec
* @brief Header for EventLocHandler.C
*/
/***********************************************************

File Name :
        EventLocHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_ASSOC_LOC_FILE messages.


Creation Date:
        15 November 1999

Modification History:
	2007/08/28, Pete Lombard: moved hypoinverse message parsing from
	hyps2ps to ec; pass file name via CMS to ec; read hypoinverse message 
	from file. All in the name of reducing CMS message size.

Usage Notes:


**********************************************************/

#ifndef event_loc_handler_H
#define event_loc_handler_H


// Various include files
#include "Connection.h"
#include "PackageAssocLocFile.h"
#include "EventCoord.h"


// Function prototype for Event Location message handler
#ifdef ELH_TEST
#include "StatusManager.h"
#include "Solution.h"

void handleEventLocMsg(Message &m, StatusManager &sm);
void DoEvent(Solution &sol, StatusManager &sm);

#else
void handleEventLocMsg(Message &m, void *arg);
#endif

#endif
