/**
* @file
* @ingroup group_event-coordinator_ec
* @brief Header for EventMessage.C
*/
/***********************************************************

File Name :
        EventMessages.h

Original Author:
        Patrick Small

Description:

        The module contains routines for transmitting various
event messages.


Creation Date:
        06 December 1999

Modification History:
	06 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef event_messages_H
#define event_messages_H

// Various include files
#include <stdint.h>
#include "DeclaredEvent.h"
#include "RTStatusManager.h"
#include "Connection.h"

int SendQuicklookSignal(RTStatusManager &sm, Connection &conn, uint32_t evid,
		    char *subject);
int SendEventSignal(RTStatusManager &sm, Connection &conn, uint32_t evid,
		    char *subject);
int SendCancel(RTStatusManager &sm, Connection &conn, DeclaredEvent &e,
	       char *auth, char *subsource, char *subject);
int SendCancelSignal(RTStatusManager &sm, Connection &conn, uint32_t evid,
		     char *subject);
int SendPrelimUpdate(RTStatusManager &sm, Connection &conn, DeclaredEvent &e,
		     char *auth, char *subsource, char *subject);
int SendFinalUpdate(RTStatusManager &sm, Connection &conn, DeclaredEvent &e,
		     char *auth, char *subsource, char *subject);

#endif
