/**
* @file
* @ingroup group_event-coordinator_ec
*/
/***********************************************************

File Name :
        Trigger.h

Original Author:
        Patrick Small

Description:


Creation Date:
        16 November 1999

Modification History:
	23 December 2007 - Pete Lombard - Update to leap seconds tntime class.


Usage Notes:


**********************************************************/

#ifndef trigger_H
#define trigger_H

// Various include files
#include "Channel.h"
#include "TimeStamp.h"

class Trigger
{
 private:

 protected:

 public:
    Channel chan;
    TimeStamp trigtime;

    Trigger();
    Trigger(const Trigger &t);
    Trigger(const Channel &c, const TimeStamp &t);
    ~Trigger();

    Trigger& operator=(const Trigger &t);
    friend int operator<(const Trigger &t1, const Trigger &t2);

};

#endif
