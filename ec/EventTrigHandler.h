/**
* @file
* @ingroup group_event-coordinator_ec
* @brief Header for EventTrigHandler.C
*/
/***********************************************************

File Name :
        EventTrigHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_EVENT_TRIG messages.


Creation Date:
        15 November 1999

Modification History:


Usage Notes:


**********************************************************/

#ifndef event_trig_handler_H
#define event_trig_handler_H


// Various include files
#include "Connection.h"


// Function prototype for Event Trigger message handler
void handleEventTrigMsg(Message &m, void *arg);


#endif
