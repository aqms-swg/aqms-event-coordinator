/**
* @file
* @ingroup group_event-coordinator_ec
*/
/***********************************************************

File Name :
        Solution.h

Original Author:
        Patrick Small

Description:


Creation Date:
        15 November 1999

Modification History:
	23 December 2007 - Pete Lombard - Update to leap seconds tntime class.


Usage Notes:


**********************************************************/

#ifndef ec_solution_H
#define ec_solution_H

// Various include files
#include "TimeStamp.h"
#include "Origin.h"
#include "Trigger.h"

#include <list>

// Definition of a trigger list
typedef std::list<Trigger> TriggerList;


// Definition of the possible solution states
typedef enum {SOL_STATE_NONE, SOL_STATE_OK, SOL_STATE_CANCELLED} solState;


class Solution
{
 private:
    Origin origin;
    TriggerList triggers;
    solState curstate;
    char SourceCode;	 // this comes from hypoinverse summary line

 protected:

 public:
    Solution();
    Solution(const Solution &s);
    ~Solution();

    int SetState(solState s);
    int SetSource(const char *lid, const char *au, const char *ss,
		  const char *algo);
    int SetTime(TimeStamp ortime);
    int SetLocation(double lat, double lon, double dep);
    int SetQuality(double q);
    int SetModelDepth(double m);
    
    int SetMaxAzmuthalGap(double gap);
    int SetNearestStationDist(double dist);
    int SetTravelTimeResidualRMS(double rms);
    int SetHorError(double herr);
    int SetVertError(double verr);
    int SetLatError(double err);
    int SetLonError(double err);
    int SetErrorEllipsoid(double ale, double dle, double le, double aie,
			  double die, double ie, double se);
    int SetReviewFlag(reviewType flag);
    int SetNumInputPhases(int num);
    int SetNumPhasesUsed(int num);
    int SetNumLocatingSPhases(int num);
    int SetModel(char *domain_code, char *processing_version_code);
    int SetCrustModel(char *model, char *type);

    int SetHypVersion(int num);
    int SetNumFirstMotions(int num);
    int SetFixedDepth(int num);
    int SetNumWeightedMags(int num);
    int SetArrivals(ChanArrivalList &arrivals);
    int SetTotalArrivals(int total);
    int SetCodas(ChanCodaList &codas);
    int SetMagnitudes(MagList &ml);
    int SetTriggers(TriggerList &tl);
    int SetSourceCode(char c);

    int GetState (solState &s);
    int GetOrigin(Origin &ori);
    int GetSource(char *lid, char *au, char *ss, char *algo);
    char GetSourceCode();

    Solution& operator=(const Solution &s);
    friend int operator==(const Solution &s1, const Solution &s2);
};

#endif
