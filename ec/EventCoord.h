/**
* @file
* @ingroup group_event-coordinator_ec
*/
/***********************************************************

File Name :
        EventCoord.h

Original Author:
        Patrick Small

Description:


Creation Date:
        12 November 1999

Modification History:
	23 December 2007 - Pete Lombard - Update to leap seconds tntime class.
	06 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef event_coord_H
#define event_coord_H

// Various include files
#include <string>
#include <list>
#include <map>
#include <stdint.h>
#include "TimeStamp.h"
#include "DatabaseLimits.h"
#include "DatabaseEC.h"
#include "RTApplication.h"
#include "Solution.h"
#include "DeclaredEvent.h"
#include "LockFile.h"


// Definition of a subject list
typedef std::list<std::string> SubjectList;

// Definition of a local region list
typedef std::list<std::string> RegionList;

// Definition of a list of declared events
typedef std::multimap<TimeStamp, DeclaredEvent, std::less<TimeStamp> > 
             EventList;

// Definition of the maximum number of state transitions
const int MAX_STATE_TRANSITIONS = 4;

// Definition of all the valid state transitions
typedef enum {TRANS_NEW_EVENT, TRANS_TIMEOUT, TRANS_CANCELLED, 
	      TRANS_DB_FAIL} transitionType;


class EventCoord : public RTApplication 
{
 private:
    char auth[DB_MAX_AUTH_LEN+1];
    char subsource[DB_MAX_SUBSOURCE_LEN+1];
    int finaldelay;
    int purgedelay;
    int quicklookdelay;
    SubjectList hypsubjects;
    SubjectList cancelsubjects;
    SubjectList evtrigsubjects;
    SubjectList subtrigsubjects;
    RegionList localRegions;
    char prelimevsubject[MAXSTR];
    char finalevsubject[MAXSTR];
    char eventsigsubject[MAXSTR];
    char cancelsubject[MAXSTR];
    char quicklooksigsubject[MAXSTR];
    char cancelsigsubject[MAXSTR];
    char hypqualfile[MAXSTR];
    char evtrigqualfile[MAXSTR];
    char subtrigqualfile[MAXSTR];
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;
    int use_magpref_stored_procs;
    int use_origin_version;
    char lock_file_name[MAXSTR];
    LockFile *lock;
    char pref_mag_char[MAXSTR];
    std::vector<string> SourceCodesVector;
    std::vector<string> SubSourceVector;

    EventList events;

    int _Cleanup();
    int _LoadHypQuality(char *filename);
    int _LoadEvTrigQuality(char *filename);
    int _LoadSubTrigQuality(char *filename);

    int _WriteEvent(DeclaredEvent &e);
    int _WriteQuicklook(DeclaredEvent &e);
    int _EventQualifies(DeclaredEvent &e);
    int _UpdateState(DeclaredEvent &ev, transitionType ttype);
    int _CheckUpdate(solType stype, Solution &sol);
    int _CreateNewEvent(solType stype, Solution &sol);
    int _GetNextTime(DeclaredEvent &ev, TimeStamp &nexttime);
    int _HasValidSolution(DeclaredEvent &ev, Solution &sol);
    int _SetNextWakeup();
    int _SetStateAndReinsert(DeclaredEvent &ev, transitionType ttype);
    int _GetEventID(uint32_t &evid);
    int _CancelQuicklook(DeclaredEvent &e);

 public:

    EventCoord();
    ~EventCoord();
    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();
    int DoEvent(solType stype, Solution &sol);
    int DoCancel(char *locid, char *src, char *subsrc, char *algo);
    int GetChanLoc(Channel &chan);
    int GetLocalRegions(RegionList &list);
    char *GetPrefMagChar();
};


#endif
