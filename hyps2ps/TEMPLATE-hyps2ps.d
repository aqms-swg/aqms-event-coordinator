#
# This is the hyps2ps parameter file
#

#  Basic Earthworm setup:
#
MyModuleId         MOD_HYP_PUB # module id for this instance of template 
RingName           HYPO_RING    # shared memory ring for input/output
LogFile            1            # 0 to completely turn off disk log file
HeartBeatInterval  30           # seconds between heartbeats
NetworkID	   NC
SourceID 	   RT1
LocAlgo		   BINDER
DurMagAlgo	   Pick_ew/Hypoinverse
#
#
# List the message logos to grab from transport ring
#              Installation       Module          Message Types
GetHypsFrom   INST_UCB        MOD_WILDCARD    # archive message
#
# Message System configuration file
MsgConfig	/home/ncss/run/params/cms.cfg
MsgSubject	/parameters/hypocenters/binder/archive
MsgDirectory	/home/ncss/run/hyp_msgs
